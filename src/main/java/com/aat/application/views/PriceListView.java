package com.aat.application.views;

import com.aat.application.data.entity.PriceListRow;
import com.aat.application.data.entity.ZJTPricelist;
import com.aat.application.data.service.PricelistService;
import com.vaadin.componentfactory.tuigrid.TuiGrid;
import com.vaadin.componentfactory.tuigrid.model.Column;
import com.vaadin.componentfactory.tuigrid.model.ColumnBaseOption;
import com.vaadin.componentfactory.tuigrid.model.Item;
import com.vaadin.componentfactory.tuigrid.model.GuiItem;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@PageTitle("Price List")
@Route(value = "pricelist", layout = MainLayout.class)

public class PriceListView extends VerticalLayout {

    private PricelistService service;

    TextField priceListText = new TextField();
    ComboBox<ZJTPricelist> pricelistCombo = new ComboBox<>("Price List");

    //	private Grid<PriceListRow> grid = new Grid<>(PriceListRow.class, false);
    TuiGrid grid;
    private BeanValidationBinder<PriceListRow> binder = new BeanValidationBinder<>(PriceListRow.class);

    private List<ZJTPricelist> m_pricelists;

    public PriceListView(PricelistService service) {
        this.service = service;
        grid = new TuiGrid();
        grid.setItems(this.getTableData());
        grid.setColumns(this.getColumns());

        setSizeFull();

        configureGrid();

//		grid = configureGrid();
        add(getPricelistPanel(), grid);

        loadPricelist();
    }

    private List<Item> getTableData() {

        List<Item> TableData = new ArrayList<>();
        List<PriceListRow> listPricingType;
        try {
            listPricingType = service.getTabulatedItems(pricelistCombo.getValue());
            List<String> headers = List.of("name", "vehicle_hours", "vehicle_km", "driver_hours", "over_head", "profit_margin");
            for (PriceListRow priceListRow :
                    listPricingType) {
                TableData.add(new GuiItem(List.of(priceListRow.getName(),
                        String.valueOf(priceListRow.getVehicleHours()),
                        String.valueOf(priceListRow.getVehicleKM()),
                        String.valueOf(priceListRow.getDriverHours()),
                        String.valueOf(priceListRow.getOverHead()),
                        String.valueOf(priceListRow.getProfitMargin())),
                        headers));

            }
        } catch (Exception e) {
            TableData = null;
        }

        return TableData;
    }

    private List<Column> getColumns() {
        List<Column> columns = List.of(
                new Column(new ColumnBaseOption(0, "Name", "name", 0, "center", "")),
                new Column(new ColumnBaseOption(1, "Vehicle Hours", "vehicle_hours", 0, "center", ""), true, "input", 10),
                new Column(new ColumnBaseOption(1, "Vehicle KM", "vehicle_km", 0, "center", ""), true, "input", 10),
                new Column(new ColumnBaseOption(1, "Driver Hours", "driver_hours", 0, "center", ""), true, "input", 10),
                new Column(new ColumnBaseOption(1, "Over Head", "over_head", 0, "center", ""), true, "input", 10),
                new Column(new ColumnBaseOption(2, "Profit Margin", "profit_margin", 0, "center", ""), true, "input", 10));
        return columns;
    }

    private void configureGrid() {
        grid.addClassName("scheduler-grid");
        grid.setSizeFull();
//		Editor<PriceListRow> editor = grid.getEditor();
//		editor.setBinder(binder);
//		editor.setBuffered(false);
//
//		grid.setColumns("name", "vehicleHours", "vehicleKM", "driverHours", "overHead", "profitMargin");
//		grid.getColumnByKey("vehicleHours").setTextAlign(ColumnTextAlign.END);
//		grid.getColumnByKey("vehicleKM").setTextAlign(ColumnTextAlign.END);
//		grid.getColumnByKey("driverHours").setTextAlign(ColumnTextAlign.END);
//		grid.getColumnByKey("overHead").setTextAlign(ColumnTextAlign.END);
//		grid.getColumnByKey("profitMargin").setTextAlign(ColumnTextAlign.END);

//		NumberField vhField = new NumberField();
//		vhField.setWidthFull();
//		binder.forField(vhField)
//			.withConverter(null)
//			.bind(PriceListRow::getVehicleHours, PriceListRow::setVehicleHours);


//		grid.addItemDoubleClickListener(e -> {
//			editor.editItem(e.getItem());
//			Component editorComponent = e.getColumn().getEditorComponent();
//			if (editorComponent instanceof Focusable) {
//				((Focusable) editorComponent).focus();
//			}
//		});
        grid.setHeaderHeight(100);
        grid.setTableWidth(750);
//		grid.setTableHeight(750);
    }

//	private Grid<List<Object>> configureGrid() {
//		final Grid<List<Object>> grid = new Grid<>();
//
//		grid.addClassName("scheduler-grid");
//		Binder<List<Object>> binder = new Binder<>();
//		grid.getEditor().setBinder(binder);
//		grid.getEditor().setBuffered(false);
//		 
//		TextField textField = new TextField();
//		binder.forField(textField).wit
//			//.bind(list -> list.g
//		
//		grid.addColumn("name");
//
//		grid.setSizeFull();
//		//grid.setColumns("name", "vehicleHours", "vehicleKM", "driverHours", "overHead", "profitMargin");
////		grid.addco
//		
//		return grid;
//	}

    private Component getPricelistPanel() {
        priceListText.setPlaceholder("New Pricelist name ...");
        priceListText.setClearButtonVisible(true);
        priceListText.setWidth("20em");

        Button addPricelistButton = new Button("Add pricelist");
        addPricelistButton.addClickListener(click -> addPricelist());
        addPricelistButton.setWidth("10em");

        Button savePricelistButton = new Button("Save pricelist");
        savePricelistButton.addClickListener(click -> savePriceList());
        savePricelistButton.setWidth("10em");

        pricelistCombo.setWidth("20em");

        pricelistCombo.addValueChangeListener(e -> updatePriceTable());

        var toolbar = new HorizontalLayout(priceListText, addPricelistButton, pricelistCombo, savePricelistButton);

        toolbar.addClassName("toolbar");

        toolbar.setAlignItems(Alignment.BASELINE);
        return toolbar;
    }

    private void addPricelist() {
        ZJTPricelist pl = new ZJTPricelist();
        pl.setName(priceListText.getValue());
        service.save(pl);
        priceListText.clear();

        m_pricelists.add(pl);
        pricelistCombo.setItems(m_pricelists);
        pricelistCombo.setValue(pl);
    }

    private void savePriceList() {
//		if (m_activeLegs == null) {
//			
//		}
//		HashMap<String, ZJTProduct> legsMap = new HashMap<>();
//		
//		m_activeLegs.stream().forEach(leg -> legsMap.put(leg.getZjt_product_id()+"", leg));
//		
//		List<Item> items = timeline.getItems();
//		
//		for (Item item : items) {
//			ZJTProduct leg = legsMap.get(item.getId());
//			
////			leg.setTriptime(item.getStart());
//			
//		}
//		//m_activeLegs.stream().filter(s -> (s.getZjt_product_id() +"").equals(it))
    }

    private void updatePriceTable() {
        List<Item> items = new ArrayList<>();
        List<String> headers = List.of("name", "vehicle_hours", "vehicle_km", "driver_hours", "over_head", "profit_margin");
        List<PriceListRow> rows = service.getTabulatedItems(pricelistCombo.getValue());

        for (PriceListRow priceListRow :
                rows) {
            items.add(new GuiItem(List.of(priceListRow.getName(),
                    String.valueOf(priceListRow.getVehicleHours()),
                    String.valueOf(priceListRow.getVehicleKM()),
                    String.valueOf(priceListRow.getDriverHours()),
                    String.valueOf(priceListRow.getOverHead()),
                    String.valueOf(priceListRow.getProfitMargin())),
                    headers));
        }
        grid.setItems(items);
        grid.addItemChangeListener(event -> {
            GuiItem item = (GuiItem) items.get(event.getRow());
            String colName = event.getColName();
            int index = item.getHeaders().indexOf(colName);
//            item.setRecordData(item.getRecordData().get(index));
            PriceListRow row = rows.get(event.getRow());
            switch (index) {
                case 1:
                    row.setVehicleHours(new BigDecimal(event.getColValue()));
                    break;
                case 2:
                    row.setVehicleKM(new BigDecimal(event.getColValue()));
                    break;

                case 3:
                    row.setDriverHours(new BigDecimal(event.getColValue()));
                    break;
                case 4:
                    row.setOverHead(new BigDecimal(event.getColValue()));
                    break;
                case 5:
                    row.setProfitMargin(new BigDecimal(event.getColValue()));
                    break;
            }
            service.updateTabulatedItem(row, pricelistCombo.getValue());
        });
        add(grid);
//		legsCombo.setItems(new ArrayList<ZJTProduct>());
//		ZJTProduct itinerary = itiCombo.getValue();
//		if (itinerary == null) {
//			return;
//		}
//		
//		m_dayOffset = 0;
//		m_lastEndDate =  LocalDateTime.now().withHour(3).withMinute(0);
//		timelineItems.clear();
//		
//		m_activeLegs = service.getItineraryLegs(itinerary);
//		
//		if (m_activeLegs == null) {
//			m_activeLegs = new ArrayList<>();
//		}
//		
//		legsCombo.setItems(m_activeLegs);
//		
//		for (ZJTProduct leg : m_activeLegs) {
//			addLegToTimeline(leg);
//			
//		}
////		m_lastEndDate = end;
////		
////		for (int i=1; i < 7; i++) {
////			Item item = new Item(start, end, "Item" + i);
////			timelineItems.add(item);
////			start = end;
////			end = start.plusMinutes(60);
////			
////		}
//		timeline.setItems(timelineItems, false);
////		timeline.setTimelineRange(m_lastEndDate.withHour(3), m_lastEndDate.withHour(22));
//		timeline.setZoomOption(1);
//		timeline.setAutoZoom(false);
//		
//		String s = service.getLegsInJSON(itinerary.getZjt_product_id());
//		
//		JsonPivotData pd = new JsonPivotData(s);
//		PivotOptions pivotOptions = new PivotOptions();
//		pivotOptions.setRows("tripleg");
//		pivotOptions.setCols("elementname");
//		pivotOptions.setAggregator(Aggregator.COUNT, "elementname");
//		pivotOptions.setCharts(false);
//		pivotOptions.setFieldsDisabled(false);
//		PivotTable table = new PivotTable(pd, pivotOptions, PivotMode.INTERACTIVE);
//		description.setValue(s);
//		
//		if (lastPivotTable != null) {
//			this.remove(lastPivotTable);
//		}
//		lastPivotTable = table;
//		this.add(table);

    }

    private void loadPricelist() {
        m_pricelists = service.getPriceLists(null);

        pricelistCombo.setItems(m_pricelists);
        pricelistCombo.setItemLabelGenerator(ZJTPricelist::getName);
    }

}
